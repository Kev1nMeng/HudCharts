package cn.tftech.hudchartssw;

/**
 * @author:zhangshihao
 * @date:2019-07-10
 * @description:
 */
public class PieDataBean {

    public PieDataBean(int value, String name) {
        this.value = value;
        this.name = name;
    }

    private int value;
    private String name;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
