package cn.tftech.hudchartssw;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.github.abel533.echarts.json.GsonOption;

public class EChartView extends WebView {

    private static final String TAG = EChartView.class.getSimpleName();

    private Context mContext;

    public EChartView(Context context) {
        this(context, null);
    }

    public EChartView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EChartView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    private void init() {
        WebSettings webSettings = getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setSupportZoom(false);
        webSettings.setDisplayZoomControls(false);
        addJavascriptInterface(new WebAppInterface(mContext), WebAppInterface.TAG);
        loadUrl("file:///android_asset/echarts.html");
    }

    /**
     * 刷新图表
     * java调用js的loadECharts方法刷新echart
     * 不能在第一时间就用此方法来显示图表，因为第一时间html的标签还未加载完成，不能获取到标签值
     * @param id 图表ID
     * @param option option
     */
    public void refreshEChartsWithOption(String id, GsonOption option) {
        if (option == null) {
            return;
        }
        String optionString = option.toString();
        String call = "javascript:loadECharts('" + id + "','" + optionString + "')";
        loadUrl(call);
    }
}