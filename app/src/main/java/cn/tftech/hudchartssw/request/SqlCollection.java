package cn.tftech.hudchartssw.request;

import android.database.sqlite.SQLiteDatabase;

/**
 * @author:zhangshihao
 * @date:2019-07-11
 * @description:
 */
public class SqlCollection {

    private static volatile SqlCollection mCollection;
    private SQLiteDatabase mDatabase;

    public static SqlCollection getInstance() {
        if (mCollection == null) {
            synchronized (SqlCollection.class) {
                if (mCollection == null) {
                    mCollection = new SqlCollection();
                }
            }
        }
        return mCollection;
    }

    private SqlCollection() {

    }

    public SQLiteDatabase getDatabase(){
        return mDatabase = SQLiteDatabase.openDatabase(ConstantCommon.SQL_PATH,null,SQLiteDatabase.OPEN_READWRITE);
    }

}
