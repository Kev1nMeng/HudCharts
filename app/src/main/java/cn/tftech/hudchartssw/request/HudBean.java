package cn.tftech.hudchartssw.request;

/**
 * @author:zhangshihao
 * @date:2019-07-15
 * @description:
 */
public class HudBean {

    private String keyId;

    private String keyType;

    private String keyTitle;

    private String keyName;

    private String keyValue;

    private String keyView;

    private String keyOrder;

    public String getKeyId() {
        return keyId;
    }

    public void setKeyId(String keyId) {
        this.keyId = keyId;
    }

    public String getKeyType() {
        return keyType;
    }

    public void setKeyType(String keyType) {
        this.keyType = keyType;
    }

    public String getKeyTitle() {
        return keyTitle;
    }

    public void setKeyTitle(String keyTitle) {
        this.keyTitle = keyTitle;
    }

    public String getKeyName() {
        return keyName;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public String getKeyValue() {
        return keyValue;
    }

    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }

    public String getKeyView() {
        return keyView;
    }

    public void setKeyView(String keyView) {
        this.keyView = keyView;
    }

    public String getKeyOrder() {
        return keyOrder;
    }

    public void setKeyOrder(String keyOrder) {
        this.keyOrder = keyOrder;
    }
}
