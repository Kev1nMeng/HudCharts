package cn.tftech.hudchartssw.request;

import io.reactivex.ObservableTransformer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * @author:zhangshihao
 * @date:2019-07-15
 * @description:
 */
public class RxUtils {

    public static <T> ObservableTransformer<T, T> netWork() {
        return upstream -> upstream.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}
