package cn.tftech.hudchartssw.request;

import android.os.Environment;

/**
 * @author:zhangshihao
 * @date:2019-07-11
 * @description:
 */
public class ConstantCommon {

    public static final String SQL_PATH =
            Environment.getExternalStorageDirectory().getAbsolutePath() +
                    "/Alarms/Hud/rember_stat_hud_pad.db";

    public static final String MAIN_HUD = "v_data_json";

    public class ChartType {
        /**
         * Treemap图表
         */
        public static final String TREEMAP = "1";
        /**
         * 柱状图
         */
        public static final String BAR_CHART = "2";
        /**
         * 饼图
         */
        public static final String PIE_CHART = "3";
        /**
         * 图表注释文字
         */
        public static final String CHART_TEXT = "4";
    }
}
